package Steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class AprendendoCucumberStep {

    @Dado("que criei o arquivo corretamente em pt")
    public void que_criei_o_arquivo_corretamente() {

        System.out.println("Passou desta Etapa");
    }

    @Quando("executar")
    public void executar() {

        System.out.println("Passou desta Etapa");
    }

    @Entao("a especificacao deve finalizar")
    public void a_especificacao_deve_finalizar() {
        System.out.println("Passou desta Etapa");

    }

    private int contador = 0;
    @Dado("que o valor do contador é {int}")
    public void queOValorDoContadorE(int int1) {

        contador = int1;
    }

    @Quando("eu incrementar em {int}")
    public void euIncrementarEm(int int1) {

        contador = contador + int1;
    }

    @Entao("o valor do contador sera {int}")
    public void oValorDoContadorSera(int int1) {
        //System.out.println(int1);
        //System.out.println(contador);
        //System.out.println(int1 == contador);
        //Assert.assertTrue(int1 == contador);
        Assert.assertEquals(int1, contador);
    }

    Date entrega = new Date();
    @Dado("que o prazo é {int}\\/{int}\\/{int}")
    public void QueOPrazoE(int dia, int mes, int ano) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, dia);
        cal.set(Calendar.MONTH, mes-1);
        cal.set(Calendar.YEAR, ano);
        entrega = cal.getTime();
    }

    @Quando("^a entrega atrasar em (\\d+) (dia|dias|mes|meses)$")
    public void aEntregaAtrasarEm(int x,String tempo) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(entrega);

        if(tempo.equals("dias")){
            cal.add(Calendar.DAY_OF_MONTH, x);
        }
        if(tempo.equals("meses")){
            cal.add(Calendar.MONTH, x);
        }
        entrega = cal.getTime();
    }

    @Entao("^a entrega sera efetuada em (\\d{2}\\/\\d{2}\\/\\d{4})$")
    public void aEntregaSeraEfetuadaEm(String data) {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = format.format(entrega);
        Assert.assertEquals(data, dataFormatada);
    }
}
