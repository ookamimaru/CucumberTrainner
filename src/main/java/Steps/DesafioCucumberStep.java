package Steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class DesafioCucumberStep {

    @Dado("^que o ticket( especial)? é (A.\\d{3})$")
    public void QueOTicketE(String tipo, String ticket) {

        System.out.println("o Ticket:" +ticket);
    }

    @Dado("que o valor da passagem é R$ {double}")
    public void QueOVValorPassagem(Double preco) {

        System.out.println("o valor é "+preco);
    }

    @Dado("^que o nome do passageiro é \"(.{5,20})\"$")
    public void QueONomeDoPassageiro(String nome) {

        System.out.println("o Nome é "+nome);
    }
    @Dado("^que o telefone do passageiro é (9\\d{3}\\-\\d{4})$")
    public void QueTelefonePassageiroE(String telefone) {
        System.out.println("o telefone: "+telefone);
    }
    @Quando("criar os steps")
    public void criar_os_steps() {
        System.out.println("passou por Aqui!");
    }
    @Então("o teste vai funcionar")
    public void o_teste_vai_funcionar() {

        System.out.println("Concluiu todo Cenairo!");
    }

}
