#language: pt

Funcionalidade: Aprender Cucumber BR
  Como um aluno
  Eu quero aprender a utilizar Cucumber
  Para que eu possa automatizar criterios de aceitação

  @aprendizado
  Cenario: Deve executar especificao
    Dado que criei o arquivo corretamente em pt
    Quando executar
    Entao a especificacao deve finalizar

  @aprendizado
  Cenario: Deve incrementar Contador
    Dado que o valor do contador é 15
    Quando eu incrementar em 3
    Entao  o valor do contador sera 18

  @aprendizadoTeste2
  Cenario: Deve incrementar Contador
    Dado que o valor do contador é 123
    Quando eu incrementar em 35
    Entao  o valor do contador sera 158

  @aprendizado
  @ManipulandoDia
  Cenario: Deve calcular atraso no prazo de entrega
    Dado que o prazo é 05/04/2018
    Quando a entrega atrasar em 2 dias
    Entao  a entrega sera efetuada em 07/04/2018

  @aprendizado
  @ManipulandoMes
  Cenario: Deve calcular atraso no prazo de entrega da China
    Dado que o prazo é 05/04/2018
    Quando a entrega atrasar em 2 meses
    Entao  a entrega sera efetuada em 05/06/2018


